<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>1. FizzBuzz | Totemic Technical Assessment</title>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: Arial, Helvetica, sans-serif;
            }
			
			.Display {
				padding:20px;
				background-color:#efefef;
			}
			
			.Display p {
				margin-top:3px;
				margin-bottom:3px;
			}
			
			.Links {
				margin-top:30px;
			}
			
			.Links a, .Links a:visited {
				color:#3344aa;
				text-decoration:none;
			}
			
			.Links a:hover {
				text-decoration:underline;
			}
        </style>
    </head>
    <body>
        <div>
            <div class="Content">
                <h1>1. FizzBuzz</h1>
				<div class="Display">
					@foreach ($Rows as $R)
					    <p>{{ $R }}</p>
					@endforeach
				</div>
                <div class="Links">
                    <a href="./">Back to Home</a>
            	</div>
            </div>
        </div>
    </body>
</html>
