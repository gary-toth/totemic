<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('fizzbuzz', 'FizzBuzzController');

Route::get('magicgettersetter', function () {
    return view('magicgettersetter');
});

Route::get('fibonacci/{N}', 'FibonacciController@GetNumber');

Route::get('social-network', function () {
    return view('social-network');
});