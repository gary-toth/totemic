<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>2. MagicGetterSetter | Totemic Technical Assessment</title>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: Arial, Helvetica, sans-serif;
            }
			
			.Display {
				padding:20px;
				background-color:#efefef;
			}
			
			.Display p {
				margin-top:0px;
				margin-bottom:15px;
			}
			
			.Links {
				margin-top:30px;
			}
			
			.Links a, .Links a:visited {
				color:#3344aa;
				text-decoration:none;
			}
			
			.Links a:hover {
				text-decoration:underline;
			}
        </style>
    </head>
    <body>
        <div>
            <div class="Content">
                <h1>2. MagicGetterSetter</h1>
				<div class="Display">
					<p>Sorry this one is not clear.</p>
					<p>
						First of all the name 'MagicGetterSetter' suggest to use the overloading magic methods i.e. <strong>__get()</strong> and <strong>__set()</strong> which is fine. 
						We can throw an exception if the property is not present in the overloaded data array in case of a 'magic get' like shown on the example below (source: <a href="https://www.php.net/manual/en/language.oop5.overloading.php#object.get">https://www.php.net/manual/en/language.oop5.overloading.php#object.get</a>)
					</p>
					<pre>
						public function __get($name)
					        {
						        echo "Getting '$name'\n";
						        if (array_key_exists($name, $this->data)) {
						            return $this->data[$name];
						        }
						
						        $trace = debug_backtrace();
						        trigger_error(
						            'Undefined property via __get(): ' . $name .
						            ' in ' . $trace[0]['file'] .
						            ' on line ' . $trace[0]['line'],
						            E_USER_NOTICE);
						        return null;
						}
					</pre>
					<p>
						Throwing exception on a 'magic set' what is confusing me a little as magic methods are for handling properties that were not declared
						so they just get created dynamically when using __set() and therefore no exception expected.
					</p>
					<p>
						The other thing I am not sure about is whether creating a 'class/trait/interface' means must use all of them or you expect me to choose
						the one that is best for this. Traits are just for re-using code and cannot be extended or implemented so I would not use them. Interfaces
						specify what methods or constants a class must use and can be extended or implemented but cannot have their methods defined. I think I 
						would just use class which can have its magic methods defined and can be easily extended.
					</p>
				</div>
                <div class="Links">
                    <a href="./">Back to Home</a>
            	</div>
            </div>
        </div>
    </body>
</html>
