<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FibonacciController extends Controller
{
	// recursive function to calculate a Fibonacci number
	private function Fibonacci($N) {
		if ($N<2) {
			$Return=$N;
		} else {
			$Return=$this->Fibonacci($N-1)+$this->Fibonacci($N-2);
		}
		return $Return; 
	}
	
    public function GetNumber($N) {
		$IntN=(int)$N;
		if ($IntN<0) {
			$Result='Invalid input: '.$N;
		} else {
			$Result='fibonacci('.$IntN.') = '.$this->Fibonacci($IntN);
		}
		return view('fibonacci',['Result'=>$Result]);
	}
}
