<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FizzBuzzController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
    	$Data=array();
		for ($N=1;$N<=20;$N++) {
			$DivisibleBy3=($N%3==0);
			$DivisibleBy5=($N%5==0);
			if ($DivisibleBy3 && $DivisibleBy5) {
				$Data[]="FizzBuzz";
			} elseif ($DivisibleBy3) {
				$Data[]="Fizz";
			} elseif ($DivisibleBy5) {
				$Data[]="Buzz";
			} else {
				$Data[]=$N;
			}
		}
		return view('fizzbuzz',['Rows'=>$Data]);
    }
}
