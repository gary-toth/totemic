<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Totemic Technical Assessment</title>
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: Arial, Helvetica, sans-serif;
            }
			
			.Links {
				margin-top:30px;
			}
			
			.Links a, .Links a:visited {
				color:#3344aa;
				text-decoration:none;
			}
			
			.Links a:hover {
				text-decoration:underline;
			}
        </style>
    </head>
    <body>
        <div>
            <div class="Content">
                <h1>Totemic Technical Assessment</h1>
				<p>
					Laravel 5.8 version<br />
					Created by <a href="mailto:gary.toth77@gmail.com">Gary Toth</a><br />
					Last updated on 23/09/2019<br />
					Code on Bitbucket <a href="https://bitbucket.org/gary-toth/totemic/src/master/">https://bitbucket.org/gary-toth/totemic/src/master/</a>
				</p>
                <div class="Links">
                    <p>
						<strong>1. FizzBuzz</strong><br />
						<a href="fizzbuzz">Show FizzBuzz</a>
					</p>
					<p>
						<strong>2. MagicGetterSetter</strong><br />
						<a href="magicgettersetter">Show MagicGetterSetter</a>
					</p>
					<p>
						<strong>3. Fibonacci</strong><br />
						<a href="fibonacci/5">Show fibonacci(5)</a><br />
						<a href="fibonacci/10">Show fibonacci(10)</a><br />
						<a href="fibonacci/1">Show fibonacci(1)</a>
					</p>
					<p>
						<strong>4. Social Network</strong><br />
						<a href="social-network">Show Social Network</a>
					</p>
            	</div>
            </div>
        </div>
    </body>
</html>
